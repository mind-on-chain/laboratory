laboratory

## Getting Started

```bash
    docker-compose up -d # Run the application stack in dev mode
```

```bash
    docker-compose logs -f # Display application logs in real time
```

```bash
    docker-compose down # Tear it all down 
```